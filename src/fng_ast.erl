-module(fng_ast).
-export([function/3, function/4, function/5, function_with_clauses/2, function_with_clauses/3, match/3, op/4,
         tuple/2, module/2, exports/2, get_arity_from_clauses/1,
         attribute/3]).

function(Line, Name, Body) when not is_list(Body) ->
    function(Line, Name, [Body]);

function(Line, Name, Body) ->
    function(Line, Name, [], [], Body).

function(Line, Name, Args, Body) ->
    function(Line, Name, Args, [], Body).

function(Line, Name, Args, When, Body) ->
    {function, Line, Name, length(Args),
     [{clause, Line, Args, When, Body}]}.

get_arity_from_clauses([H|_]) ->
    FirstClauseArgs = element(3, H),
    % TODO: check that arity is the same on all clauses
    {ok, length(FirstClauseArgs)}.

function_with_clauses(Line, Clauses) ->
    {'fun', Line, {clauses, Clauses}}.

function_with_clauses(Line, Name, Clauses) ->
    {ok, Arity} = get_arity_from_clauses(Clauses),
    {function, Line, Name, Arity, Clauses}.

match(Line, Left, Right) ->
    {match, Line, Left, Right}.

op(Line, Op, Left, Right) ->
    {op, Line, Op, Left, Right}.

tuple(Line, Items) ->
    {tuple, Line, Items}.

module(Line, Name) ->
    attribute(Line, module, Name).

attribute(Line, Attr, Value) ->
    {attribute, Line, Attr, Value}.

exports(Line, Modules) ->
    attribute(Line, export, Modules).
