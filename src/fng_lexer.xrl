Definitions.

Bool        = (true|false)

% numbers
Number      = [0-9]
RadixNumber = [0-3]?[0-9]r[0-9A-Z]+
Float       = [0-9]+\.[0-9]+([eE][-+]?[0-9]+)?

% delimiters and operators
Open        = \(
Close       = \)
OpenBlock   = {
CloseBlock  = }
OpenMap     = :{
OpenList    = [a-z\-]*\[
CloseList   = \]
Sep         = ,\n*
Endls       = (\s|\t|#(.*))*(\r?\n|;)((\s|\t|#(.*))*(\r?\n|;))*
Whites      = \s+
Tabs        = \t+
Dot         = \.\n*
Dots        = \.\.+

% string stuff
String      = [a-z\-]*"(\\\^.|\\.|[^\"])*"

% identifiers and atoms
Identifier  = [A-Z\_][a-zA-Z0-9\_]*
Form = [a-z\-]+:
Tag  = :[a-z\-]+
% if you add a symbol here add it in op_precendence in fng_parser
Atom        = [a-z\|\^\&<>~\+\-\*/\%=!@][a-zA-Z0-9\_@\|\^\&<>~\+\-\*/\%=!]*

Rules.

% numbers
{Float}                  : make_token(float,   TokenLine, TokenChars, fun erlang:list_to_float/1).
{Number}+                : make_token(integer, TokenLine, TokenChars, fun
parse_number/1).
{RadixNumber}            : make_token(integer, TokenLine, TokenChars, fun parse_radix_number/1).

% delimiters and operators
{Open}                   : make_token(open,        TokenLine, TokenChars).
{Close}                  : make_token(close,       TokenLine, TokenChars).
{OpenBlock}              : make_token(open_block,  TokenLine, TokenChars).
{OpenBlock}{Endls}       : make_token(open_block,  TokenLine, TokenChars).
{OpenMap}                : make_token(open_map,  TokenLine, TokenChars).
{OpenMap}{Endls}         : make_token(open_map,  TokenLine, TokenChars).
{CloseBlock}             : make_token(close_block, TokenLine, TokenChars).
{OpenList}               : make_list_token(open_list,   TokenLine, TokenChars).
{CloseList}              : make_token(close_list,  TokenLine, TokenChars).

{Sep}                    : make_token(sep,          TokenLine, TokenChars).
{Dot}                    : make_token(dot,          TokenLine, TokenChars).
{Dots}                   : make_token(dots,         TokenLine, TokenChars, fun identity/1).

% string stuff
{String}                 : build_string(string, TokenChars, TokenLine, TokenLen).

% identifiers and atoms
{Identifier}             : make_token(var, TokenLine, TokenChars).
{Form}                   : make_list_token(form, TokenLine, TokenChars).
{Tag}                    : make_token(tag, TokenLine, tl(TokenChars)).
{Bool}                   : make_token(boolean, TokenLine, TokenChars).
{Atom}                   : {token, atom_or_identifier(TokenChars, TokenLine)}.

% spaces, tabs and new lines
{Endls}                 : make_token(endl,  TokenLine, endls(TokenChars)).
{Tabs}                  : make_token(tab,   TokenLine, length(TokenChars)).

{Whites}                : skip_token.

Erlang code.

identity(A) -> A.

make_list_token(Name, Line, Chars) when is_list(Chars) ->
    {token, {Name, Line, lists:reverse(tl(lists:reverse(Chars)))}}.

make_token(Name, Line, Chars) when is_list(Chars) ->
    {token, {Name, Line, list_to_atom(Chars)}};
make_token(Name, Line, Chars) ->
    {token, {Name, Line, Chars}}.

make_token(Name, Line, Chars, Fun) ->
    {token, {Name, Line, Fun(Chars)}}.

endls(Chars) ->
    lists:filter(fun (C) -> C == $\n orelse C == $; end, Chars).

atom_or_identifier(String, TokenLine) ->
     case is_reserved(String) of
         true ->
            {list_to_atom(String), TokenLine};
         false ->
            {atom, TokenLine, build_atom(String, TokenLine)}
     end.

is_reserved("else")    -> true;
is_reserved(_)         -> false.

build_string(Type, Chars, Line, _Len) ->
  {Tag, Str} = lists:splitwith(fun (T) -> T /= $" end, Chars),
  StrLen = length(Str),

  StringContent = lists:sublist(Str, 2, StrLen - 2),
  String = fng_util:unescape_string(StringContent, Line),
    {token, {Type, Line, {Tag, String}}}.

build_atom([$'|_] = Atom, Line) ->
    list_to_atom(fng_util:unescape_string(
        lists:sublist(Atom, 2, length(Atom) - 2), Line));

build_atom(Atom, _Line) -> list_to_atom(Atom).

parse_number(Str) ->
    Number = list_to_integer(Str),
    {Number, nil}.

parse_radix_number(Str) ->
    [BaseStr, NumberString] = string:tokens(Str, "r"),
    Base = list_to_integer(BaseStr),
    Number = list_to_integer(NumberString, Base),
    {Number, Base}.

