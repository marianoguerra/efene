-module(fng_util).
-export([escape_string/1, unescape_string/2, pprint/2, to_ast/2,
         transform_node/3, with_transformed_nodes/4, get_erlang/1,
         handle_elses/3]).

-record(fng_state, {handlers, global_handlers, not_processed, ast, errors,
                    module}).

escape_string(String) -> escape_string(String, []).

escape_string([], Output) ->
  lists:reverse(Output);

escape_string([Char|Rest], Output) ->
  Chars = map_escaped_char(Char),
  escape_string(Rest, [Chars|Output]).

map_escaped_char(Char) ->
  case Char of
    $\\ -> [$\\, $\\];
    $/ -> [$\\, $/];
    $\" -> [$\\, $\"];
    $\' -> [$\\, $\'];
    $\( -> [$\\, $(];
    $\b -> [$\\, $b];
    $\d -> [$\\, $d];
    $\e -> [$\\, $e];
    $\f -> [$\\, $f];
    $\n -> [$\\, $n];
    $\r -> [$\\, $r];
    %$\s -> [$\\, $s];
    $\t -> [$\\, $t];
    $\v -> [$\\, $v];
    _ -> Char
  end.

unescape_string(String, Line) -> unescape_string(String, Line, []).

unescape_string([], _Line, Output) ->
  lists:reverse(Output);
unescape_string([$\\, Escaped | Rest], Line, Output) ->
  Char = map_escaped_char(Escaped, Line),
  unescape_string(Rest, Line, [Char|Output]);
unescape_string([Char|Rest], Line, Output) ->
  unescape_string(Rest, Line, [Char|Output]).

map_escaped_char(Escaped, Line) ->
  case Escaped of
    $\\ -> $\\;
    $/ -> $/;
    $\" -> $\";
    $\' -> $\';
    $\( -> $(;
    $b -> $\b;
    $d -> $\d;
    $e -> $\e;
    $f -> $\f;
    $n -> $\n;
    $r -> $\r;
    $s -> $\s;
    $t -> $\t;
    $v -> $\v;
    _ -> throw({error, {Line, fng_lexer, ["unrecognized escape sequence: ", [$\\, Escaped]]}})
  end.

get_erlang(Ast) ->
    erl_prettypr:format(erl_syntax:form_list(Ast)).

indentation_for_level(-1) -> "";
indentation_for_level(0) -> "";
indentation_for_level(1) -> "  ";
indentation_for_level(2) -> "    ";
indentation_for_level(3) -> "      ";
indentation_for_level(4) -> "        ";
indentation_for_level(N) -> lists:duplicate(N, "  ").

format(Format, Args, Level) ->
    Indent = if
        Level /= 0 andalso Level /= -1 ->
            indentation_for_level(Level);
        true -> ""
    end,

    io_lib:format("~s" ++ Format, [Indent|Args]).

pprint(Thing, Level) when is_list(Thing) ->
    pprint(Thing, Level, [], "\n");

% left op right
pprint({op, _Line, Op, Left, Right}, Level) ->
    StrLeft  = pprint(Left, Level),
    StrRight = pprint(Right, 0),
    format("~s ~s ~s", [StrLeft, Op, StrRight], 0);

% form block
pprint({form, _Line, Name, nil, Block, nil}, Level) ->
    StrBlock = pprint(Block, Level + 1),
    CloseIndent = indentation_for_level(Level),
    format("~s: {~n~s~n~s}", [Name, StrBlock, CloseIndent], Level);

% form expr
pprint({form, _Line, Name, Expr, nil, nil}, Level) ->
    format("~s: ~s", [Name, pprint(Expr, 0)], Level);

% form expr block
pprint({form, _Line, Name, Expr, Block, nil}, Level) ->
    StrExpr = pprint(Expr, 0),
    StrBlock = pprint(Block, Level + 1),
    CloseIndent = indentation_for_level(Level),

    format("~s: ~s {~n~s~n~s}", [Name, StrExpr, StrBlock, CloseIndent],
           Level);

% form block elses
pprint({form, _Line, Name, nil, Block, Elses}, Level) ->
    StrBlock = pprint(Block, Level + 1),
    StrElses = pprint(Elses, Level, [], " "),
    CloseIndent = indentation_for_level(Level),

    format("~s: {~n~s~n~s} ~s", [Name, StrBlock, CloseIndent, StrElses],
           Level);

% form expr block elses
pprint({form, _Line, Name, Expr, Block, Elses}, Level) ->
    StrExpr = pprint(Expr, 0),
    StrBlock = pprint(Block, Level + 1),
    StrElses = pprint(Elses, Level, [], " "),
    CloseIndent = indentation_for_level(Level),
    format("~s: ~s {~n~s~n~s} ~s", [Name, StrExpr, StrBlock, CloseIndent,
                                     StrElses], Level);

% form_op
pprint({form_op, _Line, Name, Left, Right}, Level) ->
    StrLeft = pprint(Left, Level),
    StrRight = pprint(Right, 0),
    format("~s ~s: ~s", [StrLeft, Name, StrRight], 0);

% final else
pprint({else, _Line, nil, nil, Block}, Level) ->
    StrBlock = pprint(Block, Level + 1),
    CloseIndent = indentation_for_level(Level),
    format("else {~n~s~n~s}", [StrBlock, CloseIndent], 0);

% else form block
pprint({else, _Line, FormName, nil, Block}, Level) ->
    CloseIndent = indentation_for_level(Level),
    StrBlock = pprint(Block, Level + 1),
    format("else ~s: {~n~s~n~s}", [FormName, StrBlock, CloseIndent], 0);

% else form expr block
pprint({else, _Line, FormName, Expr, Block}, Level) ->
    StrExpr = pprint(Expr, 0),
    StrBlock = pprint(Block, Level + 1),
    CloseIndent = indentation_for_level(Level),
    format("else ~s: ~s {~n~s~n~s}", [FormName, StrExpr, StrBlock,
                                       CloseIndent], 0);

% fcall path
pprint({fcall, _Line, FunPath, nil}, Level) ->
    format("~s()", [pprint(FunPath, Level)], 0);

% fcall path args
pprint({fcall, _Line, FunPath, Args}, Level) ->
    pprint(FunPath, Level),
    format("~s(~s)", [pprint(FunPath, 0), pprint(Args, 0, [], ", ")],
           Level);

pprint({path, _Line, Items}, Level) ->
    format("~s", [pprint(Items, 0, [], ".")], Level);

pprint({boolean, _Line, Value}, Level) ->
    format("~s", [Value], Level);

pprint({string, _Line, Type, Value}, Level) ->
    format("~s\"~s\"", [Type, escape_string(Value)], Level);

pprint({atom, _Line, Name}, Level) ->
    format("~s", [Name], Level);

pprint({var, _Line, Name}, Level) ->
    format("~s", [Name], Level);

pprint({list, _Line, Type, []}, Level) ->
    format("~s[]", [Type], Level);

pprint({list, _Line, Type, Items}, Level) ->
    format("~s[~s]", [Type, pprint(Items, 0, [], ", ")], Level);

% integer expresed with [0-9]+
pprint({integer, _Line, {Val, nil}}, Level) ->
    format("~s", [integer_to_list(Val)], Level);

% integer expressed with <base>r<number>
pprint({integer, _Line, {Val, Base}}, Level) ->
    format("~pr~s", [Base, integer_to_list(Val, Base)], Level);

pprint({float, _Line, Val}, Level) ->
    format("~p", [Val], Level);

% <left> ..+ <right>
pprint({dots, _Line, Left, Right, DotCount}, Level) ->
    StrLeft = pprint(Left, Level),
    StrRight = pprint(Right, 0),
    format("~s ~s ~s", [StrLeft, lists:duplicate(DotCount, "."), StrRight], 0);

pprint(Thing, Level) ->
    format("~p~n", [Thing], Level).

pprint([], _Level, Accum, _Sep) -> lists:reverse(Accum);

pprint([H|T], Level, Accum, Sep) ->
    Str = pprint(H, Level),

    NewAccum = if
        Sep == "" orelse Accum == [] -> [Str|Accum];
        true -> [Str|[Sep|Accum]]
    end,

    pprint(T, Level, NewAccum, Sep).

transform_node(AstNode, TopLevel, #fng_state{global_handlers=GlobalHandlers}=State) ->
    process_ast(AstNode, GlobalHandlers, TopLevel, State).

transform(AstNode, TopLevel, #fng_state{not_processed=NotProcessed, errors=Errors,
                                   ast=Ast}=State) ->
    case transform_node(AstNode, TopLevel, State) of
        {not_found, ProcessErrors} ->
            {error,
             State#fng_state{not_processed=[{AstNode,
                                             ProcessErrors}|NotProcessed]},
            nil};
        {ok, Result} ->
            {ok, State#fng_state{ast=[Result|Ast]}, Result};
        {ok, Result, ResultErrors} ->
            {ok,
             State#fng_state{ast=[Result|Ast], errors=[ResultErrors|Errors]},
             Result}
    end.

to_ast(ModName, Tree) ->
    State = #fng_state{handlers=dict:new(), global_handlers=[],
                       not_processed=[], ast=[], errors=[], module=ModName},
    to_ast(ModName, Tree, true, State).

to_ast(_ModName, [], _TopLevel, #fng_state{ast=Ast, errors=Errors,
                                           not_processed=NotProcessed}) ->
    {lists:reverse(Ast), Errors, NotProcessed};

to_ast(ModName, [{form, _Line, "lang", {atom, _, Module}, nil, nil}|T], TopLevel, #fng_state{global_handlers=GlobalHandlers}=State) ->
    NewGlobalHandlers = [{Module, lang}|GlobalHandlers],
    NewState = State#fng_state{global_handlers=NewGlobalHandlers},

    to_ast(ModName, T, TopLevel, NewState);

to_ast(ModName, [{form, _Line, "handler",
         {path, _Line1, [{atom, _L2, Module}, {atom, _L3, Function}]},
         nil, nil}|T], TopLevel,
       #fng_state{global_handlers=GlobalHandlers}=State) ->

    NewGlobalHandlers = [{Module, Function}|GlobalHandlers],
    NewState = State#fng_state{global_handlers=NewGlobalHandlers},

    to_ast(ModName, T, TopLevel, NewState);

to_ast(ModName, [H|T], TopLevel, State) ->
    {_, NewState, _} = transform(H, TopLevel, State),
    to_ast(ModName, T, TopLevel, NewState).

process_ast(Ast, Handlers, TopLevel, State) ->
    process_ast(Ast, Handlers, TopLevel, [], State).

process_ast(_Ast, [], _TopLevel, Errors, _State) ->
    {not_found, Errors};

process_ast(Ast, [{Module, Function}|T], TopLevel, Errors, State) ->
    case Module:Function(TopLevel, Ast, State) of
        skip ->
            process_ast(Ast, T, TopLevel, Errors, State);
        {error, _}=Error ->
            process_ast(Ast, T, TopLevel, [Error|Errors], State);
        {warn, _}=Warn ->
            process_ast(Ast, T, TopLevel, [Warn|Errors], State);
        {continue, NewAst} ->
            process_ast(NewAst, T, TopLevel, Errors, State);
        {ok, Result} ->
            {ok, Result, Errors}
    end.

with_transformed_nodes(_TopLevel, [], State, AllOkFun, Accum, Errors) ->
    Args = lists:reverse(Accum),
    ErrorCount = length(Errors),

    {arity, FunArity} = erlang:fun_info(AllOkFun, arity),

    if
        ErrorCount == 0 andalso FunArity == 2 -> apply(AllOkFun, [State, Args]);
        FunArity == 3 -> apply(AllOkFun, [State, Args, Errors]);
        true -> {error, {errors_in_childs, Errors}}
    end;

with_transformed_nodes(TopLevel, [NodeAst|Nodes], State, AllOkFun, Accum,
                       Errors) when is_list(NodeAst) ->
    with_transformed_nodes(TopLevel, NodeAst, State,
        fun(NewState, Items, ItemErrors) ->
            with_transformed_nodes(TopLevel, Nodes, NewState, AllOkFun,
                                   [Items|Accum], ItemErrors ++ Errors)
        end, [], []);

with_transformed_nodes(TopLevel, [NodeAst|Nodes], State, AllOkFun, Accum,
                       Errors) ->
    {NewErrors, NewState, NewAccum} = case transform(NodeAst, TopLevel, State) of
        {error, NewState1, _} ->
            {[NodeAst|Errors], NewState1, Accum};
        {ok, NewState2, NewNodeAst} ->
            {Errors, NewState2, [NewNodeAst|Accum]}
    end,

    with_transformed_nodes(TopLevel, Nodes, NewState, AllOkFun, NewAccum,
                           NewErrors).

with_transformed_nodes(TopLevel, Nodes, State, AllOkFun) ->
    with_transformed_nodes(TopLevel, Nodes, State, AllOkFun, [], []).

handle_elses(Elses, Handler, State) ->
    handle_elses(Elses, Handler, [], nil, [], State).

handle_elses([], _Handler, Accum, LastElse, Errors, _State) ->
    HasErrors = (length(Errors) /= 0),
    {lists:reverse(Accum), LastElse, lists:reverse(Errors), HasErrors};

% match the last else, it should be the last one and LastElse param should be
% nil
handle_elses([{else, _Line, _, _, _Block}=Else], Handler, Accum, nil,
             Errors, State) ->
    RemainingElses = 0,
    case Handler(Else, State, RemainingElses) of
        {ok, Ast, true} ->
            handle_elses([], Handler, Accum, Ast, Errors, State);
        {ok, Ast, false} ->
            handle_elses([], Handler, [Ast|Accum], nil, Errors, State);
        {error, Error} ->
            handle_elses([], Handler, Accum, nil, [Error|Errors], State)
    end;

% if the last else is found but there is something remaining then it's an error
handle_elses([{else, _Line, nil, nil, _Block}=Else,H|T], Handler, Accum, nil,
             Errors, State) ->
    Error = {invalid_else_clasue, {last_else_not_last, Else}},
    handle_elses([H|T], Handler, Accum, nil, [Error|Errors], State);

handle_elses([Else|T], Handler, Accum, LastElse, Errors, State) ->
    case Handler(Else, State, length(T)) of
        % NOTE: here we ignore what the handler says about the last else since
        % it shouldn't happen in the middle
        {ok, Ast, _} ->
            handle_elses(T, Handler, [Ast|Accum], LastElse, Errors, State);
        {error, Error} ->
            handle_elses(T, Handler, Accum, LastElse, [Error|Errors], State)
    end.
