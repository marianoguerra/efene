-module(efene).
-export([lang/3]).

op(TopLevel, Line, Op, Left, Right, State) ->
    fng_util:with_transformed_nodes(TopLevel, [Left, Right], State,
        fun (_NewState, [LeftAst, RightAst]) ->
            {ok, fng_ast:op(Line, op(Op), LeftAst , RightAst)}
        end).

consify([], Line) -> {nil, Line};
consify([H|T], Line) -> {cons, Line, H, consify(T, Line)}.

% else receive: expr when: <whenclause> block
handle_receive_elses({else, Line, "receive", {form_op, _, "when", Pattern, WhenExpr}, Body}, State, _) ->
    fng_util:with_transformed_nodes(false, [Pattern, WhenExpr, Body], State,
        fun (_NewState, [PatternAst, WhenAst, BodyAst]) ->
                {ok, {clause, Line, [PatternAst], [[WhenAst]], BodyAst}, false}
        end);

% else receive: expr block
handle_receive_elses({else, Line, "receive", Pattern, Body}, State, _) ->
    fng_util:with_transformed_nodes(false, [Pattern, Body], State,
        fun (_NewState, [PatternAst, BodyAst]) ->
                {ok, {clause, Line, [PatternAst], [], BodyAst}, false}
        end);

% else after: expr block
% NOTE: it has to be the last else
% NOTE: after doesn't handle when clause
handle_receive_elses({else, Line, "after", Timeout, Body}, State, 0) ->
    fng_util:with_transformed_nodes(false, [Timeout, Body], State,
        fun (_NewState, [TimeoutAst, BodyAst]) ->
                {ok, {clause, Line, [], TimeoutAst, BodyAst}, true}
        end);

handle_receive_elses(Ast, _, _) ->
    {error, {invalid_else_clasue, Ast}}.

% final else
handle_when_elses({else, Line, nil, nil, Body}, State, 0) ->
    fng_util:with_transformed_nodes(false, [Body], State,
        fun (_NewState, [BodyAst]) ->
                {ok, {clause, Line, [], [[{atom, Line, true}]], BodyAst}, true}
        end);

% else form expr block
handle_when_elses({else, Line, "when", Cond, Body}, State, _) ->
    fng_util:with_transformed_nodes(false, [Cond, Body], State,
        fun (_NewState, [CondAst, BodyAst]) ->
                {ok, {clause, Line, [], [[CondAst]], BodyAst}, false}
        end);

handle_when_elses(Ast, _, _) ->
    {error, {invalid_else_clasue, Ast}}.

% final else
handle_fn_elses({else, Line, nil, nil, Body}, State, 0) ->
    fng_util:with_transformed_nodes(false, [Body], State,
        fun (_NewState, [BodyAst]) ->
            {ok, {clause, Line, set_me_to_arity_args, [], BodyAst}, true}
        end);

% else form expr block
handle_fn_elses({else, Line, "fn", Args, Body}, State, _) ->
    case handle_fn_args(State, Args) of
        {ok, _ArgsLen, ArgsAst, WhenAst} ->
            fng_util:with_transformed_nodes(false, [Body], State,
                fun (_NewState, [BodyAst]) ->
                    {ok, {clause, Line, ArgsAst, WhenAst, BodyAst}, false}
                end);
        Error -> Error
    end;

handle_fn_elses(Ast, _, _) ->
    {error, {invalid_else_clasue, Ast}}.

handle_fn_args(_State, nil) ->
    {ok, 0, [], []};

handle_fn_args(State, {Type, _, "", Args}) when Type == tuple orelse Type == parens ->
    fng_util:with_transformed_nodes(false, [Args], State,
        fun (_NewState, [ArgsAst]) ->
            {ok, length(Args), ArgsAst, []}
        end);

handle_fn_args(State, {form_op, _, "when", {Type, _, "", Args}, WhenExpr}) when Type == tuple orelse Type == parens ->
    fng_util:with_transformed_nodes(false, [Args, WhenExpr], State,
        fun (_NewState, [ArgsAst, WhenAst]) ->
            {ok, length(Args), ArgsAst, [WhenAst]}
        end);

handle_fn_args(_State, Ast) ->
    {error, {invalid_fn_args, Ast}}.

% <funname> = fn: <arglist> <block>
% note: no elses
handle_fn(State, Name, {form, Line, "fn", Args, Body, nil}) ->
    case handle_fn_args(State, Args) of
        {ok, _ArgsLen, ArgsAst, WhenAst} ->
            fng_util:with_transformed_nodes(false, [Body], State,
                fun (_NewState, [BodyAst]) ->
                    if
                        Name == 0 ->
                            {ok, fng_ast:function_with_clauses(Line, [{clause, Line, ArgsAst, WhenAst, BodyAst}])};
                        true ->
                            {ok, fng_ast:function(Line, Name, ArgsAst, WhenAst, BodyAst)}
                    end
                end);
        Error ->
            Error
    end;

% <funname> = fn: <arglist> <block> else ...
handle_fn(State, Name, {form, Line, "fn", Args, Body, Elses}) ->
    case handle_fn_args(State, Args) of
        {ok, FunArity, ArgsAst, WhenAst} ->

            Handler = fun handle_fn_elses/3,
            {ElsesAst, LastElse, ElsesErrors, ElsesHaveErrors} = fng_util:handle_elses(Elses, Handler, State),

            LastElseWithCorrectArity = if
                LastElse /= nil ->
                    {clause, LastElseLine, set_me_to_arity_args, Conds, LastElseBody} = LastElse,
                    LastElseArgs = lists:duplicate(FunArity, {var, LastElseLine, '_'}),
                    {clause, LastElseLine, LastElseArgs, Conds, LastElseBody};
                true ->
                    nil
            end,

            fng_util:with_transformed_nodes(false, [Body], State,
                fun (_NewState, [BodyAst], FunErrors) ->
                    FunHasErrors = (length(FunErrors) /= 0),

                    FirstClause = {clause, Line, ArgsAst, WhenAst, BodyAst},

                    Clauses = if
                            LastElseWithCorrectArity /= nil ->
                                [FirstClause|ElsesAst] ++ [LastElseWithCorrectArity];
                            true ->
                                [FirstClause|ElsesAst]
                    end,

                    if
                            FunHasErrors orelse ElsesHaveErrors ->
                                {error,
                                 {error_in_childs, FunErrors ++ ElsesErrors}};
                            Name == 0 ->
                                {ok, fng_ast:function_with_clauses(Line, Clauses)};
                            true ->
                                {ok, fng_ast:function_with_clauses(Line, Name, Clauses)}
                    end
                end);
        Error ->
            Error
    end.


lang(true, {op, _Line, '=', {atom, _, Atom}, {form, _, "fn", _, _, _}=Fn}, State) ->
    handle_fn(State, Atom, Fn);

% anonymous function
lang(false, {form, _, "fn", _, _, _}=Fn, State) ->
    handle_fn(State, 0, Fn);

% when: <cond> <body> [elses]
% NOTE: we don't handle when without elses
lang(false, {form, Line, "when", Cond, Body, Elses}, State) when is_list(Elses) andalso length(Elses) > 0 ->
    AllBranches = [{else, Line, "when", Cond, Body}|Elses],
    Handler = fun handle_when_elses/3,
    {ElsesAst, LastElse, ElsesErrors, ElsesHaveErrors} = fng_util:handle_elses(AllBranches, Handler, State),

    if
        ElsesHaveErrors ->
            {error, {error_in_childs, ElsesErrors}};
        true ->
            {ok, {'if', Line, ElsesAst ++ [LastElse]}}
    end;

% receive: <pattern> <body> [elses]
lang(false, {form, Line, "receive", Cond, Body, Elses}, State) ->
    AllBranches = [{else, Line, "receive", Cond, Body}|Elses],
    Handler = fun handle_receive_elses/3,
    {ElsesAst, LastElse, ElsesErrors, ElsesHaveErrors} = fng_util:handle_elses(AllBranches, Handler, State),

    if
        ElsesHaveErrors ->
            {error, {error_in_childs, ElsesErrors}};
        LastElse /= nil ->
            {clause, _, _, TimeoutAst, TimeoutBodyAst} = LastElse,
            {ok, {'receive', Line, ElsesAst, TimeoutAst, TimeoutBodyAst}};
        true ->
            {ok, {'receive', Line, ElsesAst}}
    end;
% do: <body>
lang(false, {form, Line, "do", nil, Body, nil}, State) ->
    fng_util:with_transformed_nodes(false, Body, State,
        fun (_NewState, BodyAst) ->
            {ok, {block, Line, BodyAst}}
        end);

lang(false, {integer, Line, {Value, _Base}}, _) ->
    {ok, {integer, Line, Value}};

lang(false, {float, _, _}=Ast, _) -> {ok, Ast};
lang(false, {var, _, _}=Ast, _)   -> {ok, Ast};
lang(false, {atom, _, _}=Ast, _)  -> {ok, Ast};
lang(false, {boolean, Line, Value}, _)    -> {ok, {atom, Line, Value}};
lang(false, {string, Line, "", Value}, _) -> {ok, {string, Line, Value}};

lang(false, {list, Line, "", []}, _State) -> {ok, {nil, Line}};
lang(false=TopLevel, {list, Line, "", Items}, State) ->
    fng_util:with_transformed_nodes(TopLevel, Items, State,
        fun (_NewState, AstItems) ->
            {ok, consify(AstItems, Line)}
        end);

lang(false=TopLevel, {map_item, _, {op, ItemLine, '->', Key, Value}}, State) ->
    fng_util:with_transformed_nodes(TopLevel, [Key, Value], State,
        fun (_NewState, [KeyAst, ValueAst]) ->
            {ok, {tuple, ItemLine, [KeyAst, ValueAst]}}
        end);

lang(false=TopLevel, {map, Line, "", Items}, State) ->
    fng_util:with_transformed_nodes(TopLevel, Items, State,
        fun (_NewState, AstItems) ->
            {ok, consify(AstItems, Line)}
        end);

lang(false=TopLevel, {tuple, Line, "", Items}, State) ->
    fng_util:with_transformed_nodes(TopLevel, Items, State,
        fun (_NewState, AstItems) ->
            {ok, {tuple, Line, AstItems}}
        end);

lang(false=TopLevel, {parens, _Line, "", [Expr]}, State) ->
    lang(TopLevel, Expr, State);

% TODO: do fine checking on Left and Right on all ops to discard invalid types
lang(false=TopLevel, {op, Line, '='=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, '*'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, '/'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, '//'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, '%'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, '+'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, '-'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);

lang(false=TopLevel, {op, Line, '&'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, '|'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, '^'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);

lang(false=TopLevel, {op, Line, 'and'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, 'andd'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, 'or'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, 'orr'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);

lang(false=TopLevel, {op, Line, '<<'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, '>>'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);

lang(false=TopLevel, {op, Line, '=='=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, '==='=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, '!='=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, '!=='=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, '<'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, '<='=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, '>'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, '>='=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);

lang(false=TopLevel, {op, Line, '++'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);
lang(false=TopLevel, {op, Line, '--'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);

lang(false=TopLevel, {op, Line, '!'=Op, Left, Right}, State) ->
    op(TopLevel, Line, Op, Left, Right, State);

lang(false=TopLevel, {op, Line, '->', Left, Right}, State) ->
    fng_util:with_transformed_nodes(TopLevel, [Left, Right], State,
        fun (_NewState, [LeftAst, RightAst]) ->
            {ok, fng_ast:tuple(Line, [LeftAst, RightAst])}
        end);

lang(_, _, _) -> skip.

op('%') -> 'rem';
op('//') -> 'div';
op('<<') -> 'bsl';
op('>>') -> 'bsr';
op('<=') -> '=<';
op('===') -> '=:=';
op('!=') -> '/=';
op('!==') -> '=/=';
op('|') -> 'bor';
op('&') -> 'band';
op('^') -> 'bxor';
op('~') -> 'bnot';
op('and') -> 'andalso';
op('andd') -> 'and';
op('or') -> 'orelse';
op('orr') -> 'or';
op(Op) -> Op.
