-module(fng).
-export([tokenize/1, tokenize_str/1, parse/1, parse_str/1, run/1]).

tokenize_str(Code) ->
    case fng_lexer:string(Code) of
        {ok, Tokens, _Endline} ->
            Tokens;
        Errors ->
            throw(Errors)
    end.

tokenize(Path) ->
    Program = file_to_string(Path),
    tokenize_str(Program).

parse_tokens(Tokens) ->
    case fng_parser:parse(Tokens) of
        {ok, Tree}    -> Tree;
        {ok, Tree, _} -> Tree;
        {error, _Warnings, Errors} -> throw(Errors);
        Error -> throw(Error)
    end.

parse(Path) ->
    Tokens = tokenize(Path),
    parse_tokens(Tokens).

parse_str(Code) ->
    Tokens = tokenize_str(Code),
    parse_tokens(Tokens).

file_to_string(Path) ->
    Content = case file:read_file(Path) of
        {ok, Return} -> Return;
        {error, _Reason} = Error -> throw(Error)
    end,

    binary_to_list(Content).

get_module_name_from_path(String) ->
    File = filename:basename(String),
    ModuleNameStr = filename:rootname(File),
    list_to_atom(ModuleNameStr).

get_module_ast(Path) ->
    ModName = get_module_name_from_path(Path),
    {Ast, Errors, NotProcessed} = fng_util:to_ast(ModName, parse(Path)),
    ModuleAstNode = fng_ast:module(1, ModName),
    ExportsAst = fng_ast:exports(2, []),
    ModAst = [ModuleAstNode|[ExportsAst|Ast]],

    {ModAst, Errors, NotProcessed}.

handle_error(Path, Fun) ->
    try
        Fun()
    catch
        {error, {PLine, fng_parser, PMsg}} ->
            io:format("~s:~p ~s~n", [Path, PLine, PMsg]);
        {error, {LLine, fng_lexer, {illegal, Char}}, _} ->
            io:format("~s:~p illegal ~p~n", [Path, LLine, Char]);
        throw:Error ->
            io:format("~s: throw ~p~n~n~p~n", [Path, Error,
                                             erlang:get_stacktrace()]);
        error:Error ->
            io:format("~s: error ~p~n~n~p~n", [Path, Error,
                                             erlang:get_stacktrace()]);
        exit:Error ->
            io:format("~s: exit ~p~n~n~p~n", [Path, Error,
                                            erlang:get_stacktrace()])
    end.

show_error_or_else(_Errors, NotProcessed, Fun) ->
    NotProcessedLen = length(NotProcessed),

    if
        NotProcessedLen /= 0 ->
            io:format("not processed :~n"),
            lists:foreach(fun ({NodeAst, NodeErrors}) ->
                io:format("~n* ~s~n", [fng_util:pprint(NodeAst, 1)]),

                lists:foreach(fun ({error, {ErrorName, AstError}}) ->
                    io:format("  *~n~s: ~s~n", [ErrorName, fng_util:pprint(AstError, 2)])
                end, NodeErrors)

            end, NotProcessed);

        true -> Fun()
    end.

run(["lex", Path]) ->
    handle_error(Path, fun () -> fng_util:print(tokenize(Path)) end);

run(["tree", Path]) ->
    handle_error(Path, fun () -> fng_util:print(parse(Path)) end);

run(["ast", Path]) ->
    handle_error(Path, fun () ->
        {ModAst, Errors, NotProcessed} = get_module_ast(Path),
        
        show_error_or_else(Errors, NotProcessed, fun () ->
            io:format("ast:~n~p~nerrors:~n~p~nnot processes~n~p~n",
                [ModAst, Errors, NotProcessed])
        end)
    end);

run(["erl", Path]) ->
    handle_error(Path, fun () ->
        {ModAst, Errors, NotProcessed} = get_module_ast(Path),
        show_error_or_else(Errors, NotProcessed, fun () ->
            io:format("~s~n", [fng_util:get_erlang(ModAst)])
        end)
    end);

run(["pprint", Path]) ->
    handle_error(Path, fun () ->
        io:format("~s", [fng_util:pprint(parse(Path), 0)])
    end).
