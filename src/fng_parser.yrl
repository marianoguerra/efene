% New BSD License, part of efene, see LICENSE for details

Nonterminals
    program exprs expr fcall oper oper_f block map map_item map_items
    args more_than_1_args else_exp elses value seq tuple sform path tagval.

Terminals
    else endl sep open close open_block open_map close_block float integer atom
    boolean var string open_list close_list form dot dots tag.

Rootsymbol program.

program -> exprs : '$1'.

exprs -> expr endl       : ['$1'].
exprs -> expr endl exprs : ['$1'|'$3'].

expr -> oper_f  : '$1'.
expr -> sform : '$1'.

fcall -> path open close      : {fcall, line('$1'), simplify_path_if_one('$1'), nil}.
fcall -> path open args close : {fcall, line('$1'), simplify_path_if_one('$1'), '$3'}.
fcall -> path open more_than_1_args close : {fcall, line('$1'), simplify_path_if_one('$1'), '$3'}.

sform -> form oper_f             : {form, line('$1'), unwrap('$1'), '$2',  nil, nil}.
sform -> form block              : {form, line('$1'), unwrap('$1'),  nil, '$2', nil}.
sform -> form block elses        : {form, line('$1'), unwrap('$1'),  nil, '$2', '$3'}.
sform -> form oper_f block       : {form, line('$1'), unwrap('$1'), '$2', '$3', nil}.
sform -> form oper_f block elses : {form, line('$1'), unwrap('$1'), '$2', '$3', '$4'}.

else_exp -> else block             : {else, line('$1'), nil, nil, '$2'}.
else_exp -> else form block        : {else, line('$1'), unwrap('$2'), nil, '$3'}.
else_exp -> else form oper_f block : {else, line('$1'), unwrap('$2'), '$3', '$4'}.

elses -> else_exp       : ['$1'].
elses -> else_exp elses : ['$1'|'$2'].

block -> open_block exprs close_block : '$2'.

args -> expr : ['$1'].
more_than_1_args -> expr sep args : ['$1'|'$3'].
more_than_1_args -> expr sep more_than_1_args : ['$1'|'$3'].

oper_f -> oper             : '$1'.
oper_f -> oper form oper_f : {form_op, line('$1'), unwrap('$2'), '$1', '$3'}.

oper -> path            : simplify_path_if_one('$1').
oper -> tagval atom expr : {op, line('$1'), unwrap('$2'), '$1', '$3'}.

seq -> open_list close_list      : {list, line('$1'), unwrap('$1'), []}.
seq -> open_list args close_list : {list, line('$1'), unwrap('$1'), '$2'}.
seq -> open_list more_than_1_args close_list : {list, line('$1'), unwrap('$1'), '$2'}.

tuple -> open close                  : {tuple, line('$1'), "", []}.
tuple -> open expr sep close         : {tuple, line('$1'), "", ['$2']}.
tuple -> open more_than_1_args close : {tuple, line('$1'), "", '$2'}.

path -> tagval           : ['$1'].
path -> tagval dot path  : ['$1'|'$3'].
path -> tagval dots path :
    [{dots, line('$1'), '$1', simplify_path_if_one('$3'), length(unwrap('$2'))}].

tagval -> value tag : {tagval, line('$1'), '$1', unwrap('$2')}.
tagval -> value : '$1'.

map -> open_map close_block : {map, line('$1'), "", []}.
map -> open_map map_items close_block : {map, line('$1'), "", '$2'}.

map_item -> expr: {map_item, line('$1'), '$1'}.

map_items -> map_item: ['$1'].
map_items -> map_item sep map_items: ['$1'|'$3'].

value -> float   : '$1'.
value -> integer : '$1'.
value -> atom    : '$1'.
value -> boolean : '$1'.
value -> var     : '$1'.
value -> string  : {Tag, String} = unwrap('$1'), {string, line('$1'), Tag, String}.

value -> fcall           : '$1'.
value -> seq             : '$1'.
value -> tuple           : '$1'.
value -> map             : '$1'.
value -> open expr close : {parens, line('$1'), "", ['$2']}.

Erlang code.

unwrap({_,V})   -> V;
unwrap({_,_,V}) -> V.

line(T) when is_tuple(T) -> element(2, T);
line([H|_T]) -> element(2, H).

simplify_path_if_one(Path) ->
    PathLength = length(Path),
    FirstItem  = hd(Path),

    if
        PathLength == 1 -> FirstItem;
        true -> {path, line(FirstItem), Path}
    end.
