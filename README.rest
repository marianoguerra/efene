fenege
======

ideas
-----

* when and/or if that have no final else and return a sensitive default
* spell check strings with a tag
* generate statistics on function calls or similar

todo
----

* keep parenthesis expressions in first ast
  + normalize in second stage after precedence was handled
