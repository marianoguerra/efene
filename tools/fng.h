// New BSD License, part of fenege, see LICENSE for details
#ifndef __FNG_H__
#define __FNG_H__

struct FnOptions {
	char *output_path;
	int files_num;
	char **files;
	char *output_type;
	char mode;
	int is_test;
	char *appends;
	char *prepends;
};

#endif
